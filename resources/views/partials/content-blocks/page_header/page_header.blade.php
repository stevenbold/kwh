<div class="{{ get_row_layout() }} {{ get_row_layout() }}_{{ the_sub_field('style') }}" style="background-image:url('{{ get_sub_field('image', $option)['url'] }}')">
  <div class="container">
    {{ the_sub_field('content') }}
  </div>
</div>


<div class="section {{ get_row_layout() }}  {{ get_row_layout() }}-{{ the_sub_field('style') }}">
    @include('partials/content-blocks/extras/pre-section')
    <div  class="section-content">
        <div @if (get_sub_field('background_image')) class="bg-img"     style="background-image:url('{{ get_sub_field('background_image')['url'] }}')" @endif>
            @include('partials/content-blocks/extras/pre-inner-section')  
            
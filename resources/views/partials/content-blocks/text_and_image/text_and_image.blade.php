<div class="section">
  @include('partials/content-blocks/extras/pre-section')
  <div class="section-content section-content-{{ the_sub_field('style') }}  get_row_layout() }}">
    <div @if (get_sub_field('background_image')) class="bg-img"     style="background-image:url('{{ get_sub_field('background_image')['url'] }}')" @endif>
      @include('partials/content-blocks/extras/pre-inner-section')  
        <div class="container" data-aos="fade-up" data-aos-duration="1000">
          <div class="p-3">
            <div class="row">
              <div class="px-md-4 @if (get_sub_field('image')) col-md-6 @else col-12 @endif">
                {{ the_sub_field('content') }}
              </div>
              @if (get_sub_field('image'))
                <div class="col-md-6 reorder px-md-4">
                  <div class="bg-img" style="background-image:url('{{ get_sub_field('image')['url'] }}')"></div>                     
                </div>
              @endif
            </div>
        </div>
      </div>
      <!--@include('partials/content-blocks/extras/post-inner-section')-->
    </div>
  </div>
  @include('partials/content-blocks/extras/post-section') 
</div>

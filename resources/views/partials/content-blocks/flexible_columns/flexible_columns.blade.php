@include('partials/content-blocks/sections/section-start')
<div class="container" data-aos="fade-in" data-aos-duration="1000">
    <div class="row">
        <div class="col-{{ the_sub_field('type') }}">
            {{ the_sub_field('intro') }}
        </div>
        <div class="col-{{ the_sub_field('type') }}">
            <div class="item-container-{{ get_sub_field('type') }}">
                @if( have_rows('items') )
                    @while (have_rows('items'))  @php(the_row())
                        <div class="item">                           
                            <div class="flex-image {{ get_sub_field('image_position') }} h-100">                       
                                <div class="bg-img bg-img-contain mx-3" style="background-image:url('{{ get_sub_field('image')['url'] }}')"></div>
                            </div>                                    
                            <div class="{{ get_sub_field('image_position') }} h-100"> 
                                {{ the_sub_field('content') }}
                            </div>                                   
                        </div>                        
                    @endwhile
                @endif
            </div>
        </div>
    </div>
</div>
@include('partials/content-blocks/sections/section-end')
        
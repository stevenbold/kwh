@include('partials/content-blocks/sections/section-start')
<div class="container" data-aos="fade-in" data-aos-duration="1000">
    {{ the_sub_field('intro') }}
    @php( $content_style = get_sub_field('content_style'))
    <div class="card-deck">
        @if( have_rows('items') )
            @while (have_rows('items'))  @php(the_row())
                <div class="card card-{{ $content_style }}" style="width:400px">
                    <img class="card-img-top card-img-fix" src="{{ get_sub_field('image')['sizes']['medium'] }}" alt="Card image">
                    <div class="{{ $content_style }}">                        
                        <p class="card-text">
                            {{ the_sub_field('content') }}
                        </p>
                    </div>
                    @if ( get_sub_field('link') )
                        <div class="card-footer">
                            <a href="{{ get_sub_field('link')['url'] }}" class="btn btn-primary">{{ get_sub_field('link')['title'] }}</a>
                        </div>
                    @endif
                </div>                                     
            @endwhile
        @endif
    </div>
</div>
@include('partials/content-blocks/sections/section-end') 
        
<header class="banner bg-primary">
  <div class="container">   
    <nav class="navbar navbar-expand-lg navbar-light bg-transparent px-0d" id="primary-nav">
      <a class="navbar-brand pl-0" href="#">{{ get_bloginfo('name', 'display') }}</a>
      @if (has_nav_menu('primary_navigation'))
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarToggle" aria-controls="navbarToggle" aria-expanded="false" aria-label="Toggle navigation">
          <i class="fa fa-bars"></i>
        </button>
      @endif
      <div class="collapse navbar-collapse" id="navbarToggle">
        @if (has_nav_menu('primary_navigation'))
          {!! wp_nav_menu(['theme_location' => 'primary_navigation', 'menu_class' => 'navbar-nav']) !!}
        @endif
      </div>
    </nav>
  </div>
</header>

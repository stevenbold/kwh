<footer class="py-5">
  <div class="container">
    <div class="row text-center text-sm-left">
      <div class="col-4 py-3 offset-4 offset-sm-0 col-sm-3 col-md-2">
        <img class="img-fluid" src="{{ get_field('general_footer_logo', 'option')['url'] }}" />     
      </div>
      <div class="col-md-7 offset-md-1 col-sm-9">
        <h5>{{ get_bloginfo('name', 'display') }}</h5>
        {{ the_field('general_address', 'option') }}
        @if(get_field('general_email', 'option'))
          <p class="py-2"><a href="mailto:{{ the_field('general_email', 'option') }}"><i class="far fa-envelope"></i>&nbsp;{{ the_field('general_email', 'option') }} </a></p>
        @endif

       
      </div>
      <div class="col-md-2 text-center text-md-left">
        <h5>Quick links</h5>
        @if (has_nav_menu('footer_links'))
          {!! wp_nav_menu(['theme_location' => 'footer_links', 'menu_class' => 'footer-links']) !!}
        @endif
      </div>
    </div>
  </div>
</footer>
<?php
/**
 *  This uses ACF field group: "Page Content"
 *  If using this partial make sure that the "Page Content"
 *  field group is visible for your template
 */



$option = get_the_ID();
if(is_home()){
    //checks if it's the blog page (confusing function name)
    $option = get_option('page_for_posts');
}
if ( is_search() ) {
    $option = get_page_by_path('search');
}

?>

@if(have_rows('content_blocks', $option))
    <div class="sections">
        @while (have_rows('content_blocks', $option))  @php(the_row())

            @include('partials/content-blocks/'.get_row_layout().'/'.get_row_layout())

        @endwhile
    </div>

@else
    // no layouts found
@endif